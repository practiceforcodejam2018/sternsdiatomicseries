#include <iostream>
#include <vector>


using namespace std;


class Engine
{
    private:
        vector<int> diatomicSeriesVector;
    
        void initializeDiatomicSeriesVector()
        {
            diatomicSeriesVector.push_back(0);
            diatomicSeriesVector.push_back(1);
        }
    
    public:
        Engine()
        {
            initializeDiatomicSeriesVector();
        }
    
        int getSumAtIndex(int index)
        {
            if (index == 1 || !index)
            {
                return diatomicSeriesVector[index];
            }
            if(index & 1)                   //For index is odd.
            {
                return (getSumAtIndex(index/2) + getSumAtIndex((index/2) + 1));
            }
            return getSumAtIndex(index/2);  //For index is even.
        }
};

int main(int argc, const char * argv[])
{
    Engine e = Engine();
    cout<<e.getSumAtIndex(33)<<endl;
    return 0;
}
